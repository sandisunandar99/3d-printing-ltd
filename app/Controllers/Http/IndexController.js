'use strict'

class IndexController {
  async index({view}){
    return view.render('index.index')
  }
}

module.exports = IndexController
